#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

#include <cstdio>

template <typename T>
class ThreadsafeQueue {
private:
    std::queue<T> queue;
    std::mutex mutex;
    std::condition_variable cond;

public:
    void put(const T& item) {
        std::unique_lock<std::mutex> ulock(mutex);
        // bool wake = queue.empty(); // optimization?
        queue.push(item);
        cond.notify_one();
        // if (wake) cond.notify_one(); // optimization?
        ulock.unlock();
    }

    T take() {
        std::unique_lock<std::mutex> ulock(mutex);
        while (queue.empty())
            cond.wait(ulock);
        T item = queue.front();
        queue.pop();
        return item;
    }

    void take(T& item) {
        std::unique_lock<std::mutex> ulock(mutex);
        while (queue.empty())
            cond.wait(ulock);
        item = queue.front();
        queue.pop();
    }

    ThreadsafeQueue() = default;
    ThreadsafeQueue(const ThreadsafeQueue&) = delete;            // disable copying
    ThreadsafeQueue& operator=(const ThreadsafeQueue&) = delete; // disable assignment
};

void consumer(int id, ThreadsafeQueue<int>& queue) {
    for (int i = 0; i < 50; ++i) {
        int item = queue.take();
        std::printf("Consumer %d took %d\n", id, item);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void producer(int id, ThreadsafeQueue<int>& queue) {
    for (int i = 0; i < 75; ++i) {
        queue.put(i);
        std::printf("Producer %d put %d\n", id, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

int main() {
    ThreadsafeQueue<int> queue;

    std::thread c1(consumer, 0, std::ref(queue));
    std::thread c2(consumer, 1, std::ref(queue));
    std::thread c3(consumer, 2, std::ref(queue));
    std::thread p1(producer, 0, std::ref(queue));
    std::thread p2(producer, 1, std::ref(queue));

    c1.join();
    c2.join();
    c3.join();
    p1.join();
    p2.join();

    return 0;
}
