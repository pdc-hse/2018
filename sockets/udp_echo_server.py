#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('', 6789))

while True:
    data, addr = sock.recvfrom(1024)
    if data:
        print 'Message from %s: %s' % (addr, data.strip())
        sock.sendto(data.upper(), addr)