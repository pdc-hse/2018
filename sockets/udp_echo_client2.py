#!/usr/bin/env python
# -*- coding: utf-8 -*-

import socket
import sys

host = sys.argv[1]
port = int(sys.argv[2])
msg = sys.argv[3]

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.settimeout(3.0)
success = False
while not success:
    try:
        s.sendto(msg, (host, port))
        data = s.recv(1024)
        print 'Received', data
        success = True
    except socket.timeout:
        print 'timed out... retrying'
s.close()