#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE 2000000
#define THRESHOLD 200

void quicksort_nested(int arr[], int low, int high) {
    int i = low;
    int j = high;
    int y = 0;
    int z = arr[(low + high) / 2];
    do {
        while (arr[i] < z) i++;
        while (arr[j] > z) j--;
        if (i <= j) {
            y = arr[i];
            arr[i] = arr[j];
            arr[j] = y;
            i++;
            j--;
        }
    } while (i <= j);

    if (high-low < THRESHOLD || (j-low < THRESHOLD || high-i < THRESHOLD)) {
        if (low < j)
            quicksort_nested(arr, low, j);
        if (i < high)
            quicksort_nested(arr, i, high);
    } else {

        #pragma omp parallel sections
        {
            if (omp_get_level() <= 10) {
                printf("Level %d: %d threads\n", omp_get_level(), omp_get_num_threads());
            }
            #pragma omp section
            {
                quicksort_nested(arr, low, j);
            }
            #pragma omp section
            {
                quicksort_nested(arr, i, high);
            }
        }
    }
}

int main(void) {
    int array[SIZE] = {0};
    int i = 0;

    for (i = 0; i < SIZE; i++)
        array[i] = rand() % 10000000;

    printf("Nested parallelism: %d\n", omp_get_nested());
    printf("Max active levels: %d\n", omp_get_max_active_levels());
    printf("Thread limit: %d\n", omp_get_thread_limit());

    double start = omp_get_wtime();
    quicksort_nested(array, 0, (SIZE - 1));
    printf("Parallel Sections: %f\n", omp_get_wtime() - start);

    return 0;
}
