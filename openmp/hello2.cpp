#include <cstdio>
#include <omp.h>

int main() {
    int num_threads, thread_id;

    #pragma omp parallel private(thread_id)
    {
        thread_id = omp_get_thread_num();
        printf("%d: Hello, Parallel World!\n", thread_id);

        if (thread_id == 0) {
            num_threads = omp_get_num_threads();
            printf("Number of threads = %d\n", num_threads);
        }
    }
    return 0;
}
